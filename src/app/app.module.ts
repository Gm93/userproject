import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UserListComponent } from './components/user/user-list/user-list.component';
import { ProductListComponent } from './components/product-list/product-list.component';



import { HttpClientModule } from '@angular/common/http';
import { UserFormComponent } from './components/user/user-form/user-form.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditUserFormComponent } from './components/user/edit-user-form/edit-user-form.component';
import { DeleteUserComponent } from './components/user/delete-user/delete-user.component';
import { UserDetailsComponent } from './components/user/user-details/user-details.component';
import { UserCountComponent } from './components/user/user-count/user-count.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    UserListComponent,
    ProductListComponent,
    UserFormComponent,
    EditUserFormComponent,
    DeleteUserComponent,
    UserDetailsComponent,
    UserCountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule, // per i form, ngModel
    ReactiveFormsModule // per i form
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
