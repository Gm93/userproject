import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { } // serve per poter fare le chiamate al db. per importarlo scrivere su app.module

  private baseUrl = 'http://localhost:8080'; // parte comune dell'URL


  ///////////// CRUD OPERATION ////////////////////////////////

  getUserList(): Observable<User[]> {
    //observable è un oggetto che gestisce la chiamata
    return this.http.get<User[]>(this.baseUrl + '/userList');
  }

  getUserById(userIdFromRoute: number) {
    return this.http.get<User>(this.baseUrl + '/userList/' + userIdFromRoute);
  }

  addUser(data:any): Observable<User> { //post vuole il link più i dati, tra parentesi
    return this.http.post<User>(this.baseUrl + '/user', data);
  }

  editUser(data:any): Observable<User> {
    return this.http.put<User>(this.baseUrl + '/user', data);
  }

  deleteUser(data: any): Observable<User> {
    return this.http.delete<User>(this.baseUrl + '/delete/' + data);
  }


////////////////////////////////////////////////////////////////////

  // creo observable$ (convenzione) nel service
  private usersSubject$ = new Subject<User[]>(); // Subject nel service
  public usersList$ = this.usersSubject$.asObservable(); // Observable della lista, che è quindi presa dentro il service


  refreshList(){
    this.getUserList().subscribe((data: User[]) => {
      // i dati che arrivano li cicli, ogni dato è un user che si aggiunge in users[]
      this.usersSubject$.next(data) // i dati finiscono in userSubject che vanno poi in userList$
    });
  }

  clearList() {
    this.usersSubject$.next([]); //dico che è una lista vuota
  }

}
