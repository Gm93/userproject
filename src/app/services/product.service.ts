import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }


  private baseUrl = 'http://localhost:8080'; // parte comune dell' URL


  getUserList(): Observable<Product[]> {
    //observable è un oggetto che gestisce la chiamata
    return this.http.get<Product[]>(this.baseUrl + '/productList');
  }

}
