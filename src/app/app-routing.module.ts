import { UserDetailsComponent } from './components/user/user-details/user-details.component';
import { DeleteUserComponent } from './components/user/delete-user/delete-user.component';
import { EditUserFormComponent } from './components/user/edit-user-form/edit-user-form.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from 'src/app/components/home/home.component';
import { ProductListComponent } from 'src/app/components/product-list/product-list.component';
import { UserListComponent } from 'src/app/components/user/user-list/user-list.component';
import { UserFormComponent } from './components/user/user-form/user-form.component';

const routes: Routes = [
  {path:  '' , component: HomeComponent},
  {path:  'home' , component: HomeComponent},
  {path:  'users' , component: UserListComponent},
  {path:  'products' , component: ProductListComponent},
  {path:  'userForm' , component: UserFormComponent},
  {path:  'editForm/:userId' , component: EditUserFormComponent},
  {path:  'delete/:userId' , component: DeleteUserComponent},
  {path:  'userDetails/:userId' , component: UserDetailsComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
