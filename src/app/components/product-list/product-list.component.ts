import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit{


  products: Product[] = []; // preparo un array di user per farli vedere


  constructor(private productService: ProductService){ // richiamo il service in modo che gli passo le funzioni

  }

  ngOnInit(): void {  //posso richiamare il metodo perchè nel costruttore ho richiesto il service // inoltre con subscribe() gestisco l'evento nel momento in cui arrivano i valori, crea una chiamata e subscribe sta in attesa fino a quando non arrivano i valori, ovvero dei data di Users[]
    this.productService.getUserList().subscribe((data: Product[]) => {
      // i dati che arrivano li cicli, ogni dato è un user che si aggiunge in users[]
      data.forEach((product) => {
        this.products.push(product);
        console.log(this.products);
      });
    });


    }
}
