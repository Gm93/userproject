import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-edit-user-form',
  templateUrl: './edit-user-form.component.html',
  styleUrls: ['./edit-user-form.component.css']
})
export class EditUserFormComponent implements OnInit{

  editFormUser: FormGroup;
  userIdFromRoute!: number;

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router){
    const routeParams = this.route.snapshot.paramMap; // mappa il link della pagina
    this.userIdFromRoute = Number(routeParams.get('userId')); // questo parametro è uguale al valore della variabile con quel nome

    this.editFormUser = new FormGroup({
      "id": new FormControl(this.userIdFromRoute), // prende l'id che abbiamo cliccato
      "name": new FormControl("", Validators.required),
      "surname": new FormControl("", Validators.required),
      "age": new FormControl("", Validators.required),
    })
  }


  ngOnInit(): void {

  }

  edit(){
    this.userService.addUser(this.editFormUser.getRawValue()).subscribe()
    this.router.navigate(["/users"])
  }



}
