import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit{

  userIdFromRoute: Number;

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router){
    const routeParams = this.route.snapshot.paramMap; // mappa il link della pagina
    this.userIdFromRoute = Number(routeParams.get('userId'));
  }


  ngOnInit(): void {
    this.userService.deleteUser(this.userIdFromRoute).subscribe();
    this.router.navigate(['/users'])

  }

}
