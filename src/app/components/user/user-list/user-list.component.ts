import { UserService } from '../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {


  users: User[] = []; // preparo un array di user per farli vedere




  constructor(private userService: UserService){ // richiamo il service in modo che gli passo le funzioni

  }


  ngOnInit(): void {  //posso richiamare il metodo perchè nel costruttore ho richiesto il service // inoltre con subscribe() gestisco l'evento nel momento in cui arrivano i valori, crea una chiamata e subscribe sta in attesa fino a quando non arrivano i valori, ovvero dei data di Users[]

    /*this.userService.getUserList().subscribe((data: User[]) => {
      // i dati che arrivano li cicli, ogni dato è un user che si aggiunge in users[]
      data.forEach((user) => {
        this.users.push(user);
        console.log(this.users);
      });
    });*/


    this.userService.refreshList(); // direttamente nel service prende i dati, li propago poi qui
    this.userService.usersList$.subscribe((data: User[]) => { // mi serve farlo perchè la funzione refresh mi mette i dati dentro userList$ che è un observable generato da userSubject (vedi nel service)
      this.users = data;
    });

    }


    // funzione di refresh del componente

    refreshList() {
      this.userService.refreshList(); // con un unica chiamata spande i dati a tutti i componenti, quindi anche a count
    }

    clearList() {
      this.userService.clearList();
    }


  }

