import { Router, RouterModule } from '@angular/router';
import { UserService } from './../../../services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit{

  formUser: FormGroup;

  constructor(private userService: UserService, private router: Router){
    this.formUser = new FormGroup({
      "name": new FormControl("", Validators.required),
      "surname": new FormControl("", Validators.required),
      "age": new FormControl("", Validators.required),
    })
  }

  ngOnInit(): void {

  }

  register() {
    console.log(this.formUser.getRawValue())
    this.userService.addUser(this.formUser.getRawValue()).subscribe()
    this.router.navigate(["/users"])  // per far tornare alla pagine principale dopo aver registrato
  }
}
