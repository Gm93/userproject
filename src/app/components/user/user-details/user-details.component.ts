import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {


user!:User; // dettaglio utente

userIdFromRoute: number; //id utente preso dalla rotta

  constructor(private route: ActivatedRoute, private userservice: UserService) {
    const routeParams = this.route.snapshot.paramMap; // mappa il link della pagina
    this.userIdFromRoute = Number(routeParams.get('userId'));
  }



  ngOnInit(): void {
      this.userservice.getUserById(this.userIdFromRoute).subscribe((user: User) => { //metodo del service da cui prendo i dati
        this.user = user;
      });

  }

}
