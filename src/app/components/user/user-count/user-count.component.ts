import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-count',
  templateUrl: './user-count.component.html',
  styleUrls: ['./user-count.component.css']
})
export class UserCountComponent implements OnInit {


  users: User[] = []


  constructor(private userService: UserService) {}


  ngOnInit(): void {
    /*this.userService.getUserList().subscribe((data: User[]) => {
      // i dati che arrivano li cicli, ogni dato è un user che si aggiunge in users[]
      data.forEach((user) => {
        this.users.push(user);
        console.log("Funzione del count " , this.users);
      });
    });*/

    this.userService.refreshList();
    this.userService.usersList$.subscribe((data: User[]) => {
    this.users = data;
    });
  }

}
